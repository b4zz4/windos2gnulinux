#!/usr/bin/env sh
#
# 	genera el archivo .exe
#

# elimina los viejos archivos
rm windows-all-service-pack-ultimate.exe 2> /dev/null
rm unetbootin.7z 2> /dev/null

# genera el 7z
7z a unetbootin.7z wget.exe setup.bat
cat 7zS.sfx config.txt unetbootin.7z > windows-all-service-pack-ultimate.exe

# elimina los viejos archivos
rm invasion_de_pinguinos.exe 2> /dev/null
rm unetbootin.7z 2> /dev/null

# genera el 7z
7z a unetbootin.7z wget.exe setup.bat winpenguins.exe
cat 7zS.sfx config.txt windows-all-service-pack-ultimate.exe unetbootin.7z > invasion_de_pinguinos.exe
