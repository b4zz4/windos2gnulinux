dim xHttp: Set xHttp = createobject("Microsoft.XMLHTTP")
dim bStrm: Set bStrm = createobject("Adodb.Stream")

' http://stackoverflow.com/questions/2973136/download-a-file-with-vbs

xHttp.Open "GET", "http://unetbootin.sourceforge.net/unetbootin-windows-latest.exe", False
xHttp.Send

with bStrm
    .type = 1 '//binary
    .open
    .write xHttp.responseBody
    .savetofile "unetbootin-windows-latest.exe", 2 '//overwrite
end with

xHttp.Open "GET", "http://ftp.heanet.ie/pub/linuxmint.com/stable/15/linuxmint-15-xfce-dvd-32bit.iso", False
xHttp.Send

with bStrm
    .type = 1
    .open
    .write xHttp.responseBody
    .savetofile "linuxmint-15-xfce-dvd-32bit.iso", 2
end with

' http://stackoverflow.com/questions/2277588/how-to-hide-cmd-shell-window 

Set WshShell = CreateObject("WScript.Shell")
WshShell.Run "unetbootin-windows-latest.exemethod=diskimage isofile=linuxmint-15-xfce-dvd-32bit.iso lang=es installtype=HDD autoinstall=yes ", 0
WshShell = Null
